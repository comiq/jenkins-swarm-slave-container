FROM ubuntu:latest

RUN apt-get update -y && \
  apt-get install -y apt-transport-https && \
  apt-get install -y wget && \
  apt-get install -y curl && \
  apt-get install -y openjdk-8-jre && \
  apt-get install -y docker.io && \
  apt-get install -y gnupg && \
  apt-get clean

RUN curl -s https://packages.cloud.google.com/apt/doc/apt-key.gpg | apt-key add -
RUN echo "deb https://apt.kubernetes.io/ kubernetes-xenial main" > /etc/apt/sources.list.d/kubernetes.list
RUN apt-get update && \
  apt-get install -y kubectl && \
  apt-get clean

RUN curl -O https://storage.googleapis.com/kubernetes-helm/helm-v2.11.0-linux-amd64.tar.gz && \
  tar xvfz helm-v2.11.0-linux-amd64.tar.gz && \
  mv linux-amd64/helm /usr/local/bin/helm && \
  rm -rf helm*

RUN wget https://repo.jenkins-ci.org/releases/org/jenkins-ci/plugins/swarm-client/3.9/swarm-client-3.9.jar

COPY entrypoint.sh entrypoint.sh
RUN chmod ugo+x entrypoint.sh

ENV EXECUTORS=1
ENV SLAVE_NAME=slave

USER root
RUN cat /etc/passwd | awk '{ if ( NR == 1 ) print "root:x:0:0:root:/var/jenkins_home:/bin/bash" ; else print $0 }' > /etc/passwd2 && \
  cat /etc/passwd2 > /etc/passwd && \
  rm /etc/passwd2
VOLUME /var/jenkins_home

ENTRYPOINT [ "/entrypoint.sh" ]

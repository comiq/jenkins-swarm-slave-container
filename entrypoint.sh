#!/bin/sh

# Configure Kubectl if we are running in kube
if [ -d /var/run/secrets/kubernetes.io ]; then
  kubectl config set-cluster default-cluster --server=https://kubernetes.default.svc
  kubectl config set-cluster default-cluster --certificate-authority="/var/run/secrets/kubernetes.io/serviceaccount/ca.crt"
  kubectl config set-credentials default-user --token=$(cat /var/run/secrets/kubernetes.io/serviceaccount/token)
  helm init

  mkdir /var/jenkins_home/.ssh
  ln -s /var/run/secrets/id_rsa /var/jenkins_home/.ssh/id_rsa

  ssh-keyscan -t rsa gitlab.com >> /var/jenkins_home/.ssh/known_hosts
fi

java -jar swarm-client-3.9.jar -master http://master:8080 -username admin -password admin -name $SLAVE_NAME -executors $EXECUTORS
